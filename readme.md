This file is a english-polish vocabulary of terms used in the theory of dynamical systems.  


qph
===

qph = Qt Phrase book   


Format   

http://docs.translatehouse.org/projects/translate-toolkit/en/latest/formats/qt_phrase_book.html



    <!DOCTYPE QPH><QPH>
      <phrase>
        <source>Source</source>
        <target>Target</target>
        <definition>Optional definition</definition>
      </phrase>
    </QPH>


Git
===

cd existing_folder  
git init  
git remote add origin git@gitlab.com:adammajewski/dynamical_system_englis2polish_QT_phrase_book_qph.git  
git add .  
git commit  
git push -u origin master  
